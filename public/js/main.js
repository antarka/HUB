///<reference path='../../typings/tsd.d.ts' />
(function () {
    var APP = angular.module("Ethernium", [
      'angular-loading-bar',
      'ngAnimate',
      'HUB_Auth',
      'HUB_Configuration',
      'HUB_Routing',
      'HUB_Socket',
      'HUB_Notify',
      'HUB_Addfriends',
      'HUB_API_Friends'
    ]);

    APP.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
        var original = $location.path;
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };
    }]);

    APP.controller("PrimaryController", function ($scope, templateURL, config, $location) {

        $scope.init = function (layout, template) {
            templateURL = "transpile/" + layout + "/" + template;
            config.layout = layout;
            config.template = template;
            $scope.layout = layout;
        };
        $scope.changeInclude = function (templateName,locationPath) {
            if(locationPath == null)
              locationPath = false;
            if(locationPath) {
              if(templateName == "auth") {
                templateName = "";
              }
              $location.path(templateName,false);
            }
            templateURL = "transpile/" + $scope.layout + "/" + templateName;
            config.template = templateName;
        };
        $scope.getInclude = function () {
            return templateURL;
        };

        // Popup
        $scope.popupOpen = false;
        $scope.popupTemplate = "popup/add_friends";
        $scope.closePopup = function() {
          $scope.popupOpen = false;
        }
        $scope.changePopup = function (template) {
          if(!$scope.popupOpen) {
            $scope.popupTemplate = template;
            $scope.popupOpen = true;
          }
        };
        $scope.getPopup = function () {
            return $scope.popupTemplate;
        };
    });

    APP.controller("AuthController",function($scope, $location) {
      if($location.path() != "/") {
        $location.path("",false);
      }

      this.inviteMode = function() {
        console.log("invite mode");
      }
    });

    APP.controller("HomeController", function ($scope, $http, userData, $timeout, socket, config, notify, $location) {
        $scope.config = config;

        if($location.path() != "/home") {
          $location.path("home",false);
        }

        if (userData.login == null || userData.login == undefined) {
            $http.get('/auth/userinfo').success(function (data) {
                if (data.login != undefined && data.login != null) {
                    userData.login = data.login;
                    userData.email = data.email;
                    userData.avatar = data.avatar;
                    userData.level = data.level;
                    userData.right = data.right;
                    $scope.userData = userData;
                }
            });
        }

        $scope.userData = userData;
        $scope.dateNow = Date.now();
        $timeout.cancel(countUp);
        var countUp = function () {
            $scope.dateNow = Date.now();
            $timeout(countUp, 1000);
        };
        $timeout(countUp, 1000);

        $scope.logout = function () {
            socket.emit('prevent_logout',null);
            //$location.path('logout');
            document.location.href = "/logout";
        };

        socket.on('emptySession_loginErr',function(data) {
          document.location.href = "#/home";
        });

    });

})();
