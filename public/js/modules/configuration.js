(function () {
    var APP = angular.module("HUB_Configuration", []);

    APP.value('userData', {
        login: null,
        email : null,
        avatar: null,
        level : 0,
        right: 0
    });
    APP.value('templateURL', null);

    APP.value('config', {
        layout: null,
        template: null
    });
})();
