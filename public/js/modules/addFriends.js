(function () {
    var AddFriends = angular.module("HUB_Addfriends", []);

    AddFriends.controller("AddFriends", function ($scope, socket) {

        $scope.results = [];
        this.login = "";
        this.searchFriends = function () {
            socket.emit('clientRequest_searchFriends', {
              login: this.login
            });
        };

        socket.on('serverRespond_FriendResult', function (data) {
            if (data.results != false) {
                $scope.results = data.results;
            }
            else {
              $scope.results = [];
            }
        });

    });

})();
