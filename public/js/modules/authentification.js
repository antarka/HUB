///<reference path='../../../typings/tsd.d.ts' />
(function () {
    var APP = angular.module("HUB_Auth", ['Equals']);

    APP.controller("Auth_menu", function ($scope, $http, userData) {
        $scope.inviteMode = function () {
            $http.post('/auth/register/', {
                formData: $scope.formData
            }).success(function (data, status, headers, config) {
                if (data.state == "ok") {
                    console.log("ok");
                    userData.login = data.nfo.login;
                    document.location.href = "/home";
                }
                else {
                    console.log("error");
                }
            }).error(function (data, status, headers, config) {
                console.log("error");
            });
        };
    });

    APP.controller('inscriptionController', ['$scope', '$http', 'userData', function ($scope, $http, userData) {
            $scope.formData = {};
            $scope.reset = function () {
                $scope.formData = {};
                $scope.inscription.$setPristine();
            };
            $scope.submit = function () {
                console.log($scope.formData);
                $http.post('/auth/register/', {
                    formData: $scope.formData
                }).success(function (data, status, headers, config) {
                    if (data.state == "ok") {
                        console.log("ok");
                        userData.login = data.nfo.login;
                        userData.right = data.nfo.right;
                        userData.level = data.nfo.level;
                        userData.avatar = data.nfo.avatar;
                        document.location.href = "#/home/";
                    }
                    else {
                        console.log("error");
                        $scope.formData = {};
                        $scope.inscription.$setPristine();
                    }
                }).error(function (data, status, headers, config) {
                    console.log("error");
                });
            };
        }]);

    APP.controller('loginController', ['$scope', '$http', 'userData', function ($scope, $http, userData, $location) {
            $scope.formData = {};
            $scope.reset = function () {
                $scope.formData = {};
                $scope.login.$setPristine();
            };
            $scope.submit = function () {
                $http.post('/auth/login/', {
                    formData: $scope.formData
                }).success(function (data, status, headers, config) {
                    if (data.state == "ok") {
                        userData.login = data.nfo.login;
                        userData.right = data.nfo.right;
                        userData.level = data.nfo.level;
                        userData.avatar = data.nfo.avatar;
                        document.location.href = "#/home/";
                    }
                    else {
                        $scope.formData = {};
                        $scope.login.$setPristine();
                    }
                }).error(function (data, status, headers, config) {
                });
            };
        }]);
})();
