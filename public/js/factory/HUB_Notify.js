(function () {
    var Notify = angular.module("HUB_Notify", []);

    Notify.factory('notify', function () {
        var max_notifications = 4;
        var actu_notifications = 0;
        return {
            send : function (title, message, icon) {
                if (!("Notification" in window)) {
                }
                else if (Notification.permission === "granted") {
                  if(actu_notifications <= max_notifications) {
                    var notification = new Notification(title, {
                        body: message,
                        icon: icon
                    });

                    notification.onshow = function() {
                      actu_notifications++;
                    }

                    notification.onclick = function () {
                      notification.close();
                    };

                    notification.onclose = function() {
                      actu_notifications--;
                    }

                    var myNotif = notification;
                    var timeOut = setTimeout(function(){
                      myNotif.close();
                      clearTimeout(timeOut);
                    }, 3000);
                  }
                }
                else if (Notification.permission !== 'denied') {
                    Notification.requestPermission(function (permission) {
                        if (!('permission' in Notification)) {
                            Notification.permission = permission;
                        }
                        if (permission === "granted") {
                          if(actu_notifications <= max_notifications) {
                            var notification = new Notification(title, {
                                body: message,
                                icon: icon
                            });

                            notification.onshow = function() {
                              actu_notifications++;
                            }

                            notification.onclick = function () {
                              notification.close();
                            };

                            notification.onclose = function() {
                              actu_notifications--;
                            }

                            var myNotif = notification;
                            var timeOut = setTimeout(function(){
                              myNotif.close();
                              clearTimeout(timeOut);
                            }, 3000);
                          }
                        }
                    });
                }
            }
        }
    });

    document.addEventListener('DOMContentLoaded', function () {
      if (Notification.permission !== "granted")
        Notification.requestPermission();
    });

})();
