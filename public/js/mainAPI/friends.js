(function () {
    var API_Friends = angular.module("HUB_API_Friends", [
      'HUB_Socket',
      'HUB_Notify'
    ]);

    API_Friends.controller("Friends_Sub", function ($scope, $document, $timeout, socket) {

        $scope.mouseIcon = false;
        $scope.mouseIn_sous_menu = false;
        $scope.options_menu = false;

        var close_sousmenu_delay = function () {
            if(!$scope.mouseIn_sous_menu && $scope.options_menu && !$scope.mouseIcon) {
              $scope.options_menu = false;
              $timeout.cancel(close_sousmenu_delay);
            }
            else {
              $timeout(close_sousmenu_delay, 1000);
            }
        };

        $scope.open_sousmenu = function () {
            if (!$scope.mouseIn_sous_menu)
                $timeout(close_sousmenu_delay, 1000);
                $scope.options_menu = !$scope.options_menu;
        };

        $scope.sousmenu_leave = function() {
          $scope.mouseIn_sous_menu = false;
          $timeout(close_sousmenu_delay, 1000);
        }

        $document.bind('click', function (event) {
          if(!$scope.mouseIn_sous_menu && $scope.options_menu && !$scope.mouseIcon)
            $scope.options_menu = false;
        });

        $scope.deleteFriends = function(friendName) {
          socket.emit('clientRequest_deleteFriend',{
            friendName: friendName
          });
        }
    });

    API_Friends.controller("Messages", function ($scope) {
        $scope.messages = [
            {login: "captainfive",avatar: "nova2"},
            {login: "shqiptar",avatar: "volt"}
        ];
    });

    API_Friends.controller("End", function ($scope, $document, socket, notify) {
        $scope.openState = false;
        $scope.mouseIn = false;
        $scope.mouseIn_button = false;

        $document.bind('click', function () {
            if (!$scope.mouseIn && $scope.openState && !$scope.mouseIn_button && !$scope.popupOpen)
                $scope.openState = false;
        });

        $scope.open = function () {
            $scope.openState = !$scope.openState;
        };

        $scope.friendsList = [];
        socket.emit('clientRequest_friends', {});
        socket.on('serverRespond_friends', function (data) {
            $scope.friendsList = data.friends;
        });

        socket.on('friends_nowOnline', function (data) {
            var avatar = "/img/avatar/"+data.avatar+".png";
            notify.send(data.login, "Est maintenant en ligne!", avatar);
            for (var k in $scope.friendsList) {
                var env = $scope.friendsList[k];
                if (env.login == data.login) {
                    env.online = true;
                }
            }
        });

        socket.on('friends_nowOffline', function (data) {
            for (var k in $scope.friendsList) {
                var env = $scope.friendsList[k];
                if (env.login == data.login) {
                    env.online = false;
                }
            }
        });

        socket.on('serverRespond_looseFriend',function(data) {
          for (var k in $scope.friendsList) {
              var env = $scope.friendsList[k];
              if (env.login == data.friendName) {
                  $scope.friendsList[k] = null;
                  delete $scope.friendsList[k];
              }
          }
        });

    });

})();
