(function () {
    var Routing = angular.module("HUB_Routing", ['ngRoute']);

    Routing.controller('routing_auth_auth', function ($scope, userData, $location) {
      if (userData.login != null) {
        $location.path('home');
      }
    });

    Routing.config(['$routeProvider', function ($routeProvider) {
      $routeProvider
      .when('/', {
          templateUrl: 'partials/auth/auth',
          controller: "routing_auth_auth"
      })
      .when('/inscription', {
          templateUrl: "partials/auth/inscription"
      })
      .when('/login', {
          templateUrl: "partials/auth/login"
      })
      .when('/home', {
          templateUrl: 'partials/main/home'
      })
      .when('/news', {
          templateUrl: 'partials/main/news'
      })
      .when('/support', {
          templateUrl: 'partials/main/support'
      })
      .when('/option', {
          templateUrl: 'partials/main/option'
      })
      .when('/profil', {
          templateUrl: 'partials/main/profil'
      })
      .when('/logout', {
          templateUrl: 'logout'
      })
      .otherwise({
          redirectTo: '/'
      });
    }]);

})();
