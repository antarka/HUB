# Feuille de route

### 0.0.3

> Nginx

- [ ] Mise en place de l'https.

> Client Side

- [x] Intégration des popups (1.0).
- [x] Migration de CSS à **Stylus**.
- [x] Amélioration des notifications.
- [x] Correction de l'animation d'ng-include.
- [x] Amélioration des transitions de la branche **'main'**.
- [x] Popup => mise en place de l'animation de transition.
- [x] Correction de la fermeture du menu d'action dans la liste d'amis.
- [x] Intégration de roboto en local.
- [x] Remettre les dépendances Angular en local!
- [ ] Problème avec l'opacité du sous-menu d'un ami.
- [ ] Revoir le support du jeu, faire le sous-menu Games
- [ ] Ajout d'une section haute dans la liste d'amis (avec pseudo, online/absent/offline, et tagID éventuellement).
- [ ] Afficher la version.
- [ ] **Ajouter** un ami. - (popup & action).
- [ ] **Pending** - popup & action.
- [ ] **Supprimer** un ami.
- [ ] Gestion des notifications. (liste d'amis).
- [ ] Option du HUB (VISUEL).
- [ ] Eviter la fermeture de la liste d'amis lorsqu'on quitte une popup!

> Client side - AngularJS

- [x] Séparer les scripts en plusieurs morceaux, amélioration de comment sont déclarer les variables dans le $scope.
- [x] Création d'une factory NotifyME.
- [x] Création d'une factory Socket.
- [x] Mise en place de vérification du **$location.path()**

> Server side

- [x] Développement d'un script .JS **permettant la mise en place d'une BDD propre et initiale**.
- [x] Amélioration des websockets avec des class Typescript.
- [x] Crypter les mots de passe en SHA1.
- [ ] Développement de **l'invite mode!**
- [ ] Support de plusieurs type de lancemement dans le make.sh (**nettoyer la bdd ?**)

> BDD

- [x] Amélioration de la structure des amies (Mise en place des notifications)

-----------------------

### 0.0.4

> Remettre groupe et messages dans main.jade

- [ ] Développement d'un profil 'basique'.
- [ ] Ajout d'une branche 'Discover'
- [ ] Mettre en place une page avec feuille de route dans discover!
- [ ] Group API (en liaison avec les amies).
- [ ] Améliorer la reconnexion coté client.
- [ ] Développement du module support.
- [ ] Amélioration du responsive design du HUB.
- [ ] Développement concret de la structure des jeux.
- [ ] Amélioration du changeTemplate grâce à location.path()
- [ ] Amélioration du prototype de supression dans un objet.

> Server Side

- [ ] Group API - Server side
- [ ] Ajouter un mode "en attente" coté socket. (permet d'éviter une déco/reco lors d'un refresh de page).

> BDD

- [ ] Mise en place de la blacklist

> Retro

- [ ] Mettre un cheat IE en place (IE <10)
- [ ] Demander mise à jour du navigateur si  (version > +10) [Chrome, Firefox, Edge, Opera]
- [ ] Problème de session sous IE ?

-------------------

### 0.0.5 +

 - [ ] Re-mise en place de passport.JS pour des stratégies d'auth plus 'global'.
 - [ ] Développement des lobbys.
