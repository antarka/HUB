///<reference path='typings/tsd.d.ts' />
let config = require('./config.json');
let path  = require("path");
let fs    = require('fs');

let io      = require('socket.io');
let express = require('express');
let colors  = require('colors');

let bodyparser    = require("body-parser");
let session       = require('express-session');
let favicon       = require('serve-favicon');
let cookie        = require('cookie');
let cookieParser  = require('cookie-parser');
let async         = require('async');

let debug = require('debug')('hub');
let Q = require('q');

// Start express.
let app = express();
let server = app.listen(process.env.PORT || config.port, process.env.IP || config.ip);
let websocket = io(server);

let dataSchema = require('./core/db/dataSchema');
let mongoose = require('./core/db/conf')("localhost","27017","HUB");

// Configure view engine.
app.set('views',path.join(__dirname,'views'));
app.set('view engine','jade');

// Serve favicon
app.use(favicon(__dirname + '/public/favicon.ico'));

// Use public for front dependencies.
app.use(express.static( path.join(__dirname,'/public') ));
app.use("/template",express.static( path.join(__dirname,'/views') ));
app.use("/ngDirective",express.static( path.join(__dirname,'/public/angular/directives') ));
app.use("/imgSocial",express.static( path.join(__dirname,'/public/img/social') ));

// Use bodyparser for post & get request.
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

// On configure la session.
let COOKIE_SECRET = 'secret';
let MongoStore = require('connect-mongo')(session);

let sessionStore = new MongoStore({ mongooseConnection: mongoose.connection });
let cookieMiddleware = cookieParser(COOKIE_SECRET);
let sessionMiddleware = session({
  name : 'HUB_APP',
  secret: COOKIE_SECRET,
  resave: true,
  store: sessionStore,
  saveUninitialized: true,
	rolling : true,
  cookie: {
		path: '/',
		httpOnly: true,
		secure: false,
		maxAge: 10000000
	}
});

app.use(cookieMiddleware);
app.use(sessionMiddleware);

// Create mongoDB schema for our APP.
let createMongoSchema = (dataStore,name) => {
  let instance = new mongoose.Schema(dataStore);
  return mongoose.model(name,instance,name);
}

let mongoDB = {
  utilisateurs : createMongoSchema(dataSchema.utilisateurs,"utilisateurs")
}

app.use(function(req,res,next) {
  req.db = {
    utilisateurs : mongoDB.utilisateurs
  };
  next();
});

app.use('/',require('./core/routes/global'));
app.use('/',require('./core/routes/authentification'));
app.use('/',require('./core/routes/partials'));

let ios = require('socket.io-express-session');
websocket.use(ios(sessionMiddleware));

// Error 404
app.use(function(req,res) {res.status(404).render('error/404');});

import User from "./websockets/user";
User.setBdd(mongoDB);
User.io = websocket;

websocket.on("connection",(socket) => {

  if(socket.handshake.session.login == undefined || socket.handshake.session.login == null) {
    console.log("EmptySocket detected");
    socket.emit("emptySession_loginErr",{
      code : 0
    });
  }
  let user = new User(socket.id,socket.handshake.session);

  if(user.haveRight()) {
    mongoDB.utilisateurs.find({login: user.name}, {},function(err,r){
      var env = r[0].friends[0];
      if(env != null) {
        if(env.length != 0){
          async.forEachOf(env, function (value, key, callback) {
              if(User.sendEvents(key,"friends_nowOnline",{login : user.name, avatar: user.avatar})) {
                callback(null);
              }
          },function(err) {
            if (err) console.error(err.message);
            // All OKAY !
          });
        }
        else {
          console.log("length null");
        }
      }
    });
  }

  socket.on('prevent_logout',function() {
    if(user.name != null) {
      mongoDB.utilisateurs.find({login: user.name}, {},function(err,r){
        var env = r[0].friends[0];
        if(env != null) {
          if(env.length != 0){
            async.forEachOf(env, function (value, key, callback) {
                if(User.sendEvents(key,"friends_nowOffline",{login : user.name})) {
                  callback(null);
                }
            },function(err) {
              if (err) console.error(err.message);
              // All OKAY !
            });
          }
        }
      });
    }
    else {
      console.log("Username is null!");
    }
  });

  // Lorsque l'utilisateur ce déconnecte !
  socket.on('disconnect',function(){
    // Destroy class!
    user.destructor();
    user = null;
  });

  // Le client nous demande ça liste d'amies.
  socket.on('clientRequest_friends',(data)=>{
    if(user.haveRight()) {
      mongoDB.utilisateurs.find({login: user.name}, {},function(err,r){
        let env = r[0].friends[0];
        if(env != null) {
          if(env.length != 0){

            let info = [];
            async.forEachOf(env, function(value,key, callback) {
                User.findUserbyName(key).then( function(res) {
                  User.findConnectedUser(res.login).then( function(online) {
                    info.push({
                      login : res.login,
                      avatar : res.avatar,
                      online : online,
                      level : res.level
                    });
                    callback(null);
                  });
                });
            }, function(err) {
              if (err) console.error(err.message);
              socket.emit('serverRespond_friends',{
                friends: info
              });
            });

          }
        }
      });
    }
  });


  socket.on('clientRequest_searchFriends',(data) => {
    if(data.login != "") {
      mongoDB.utilisateurs.find({login:new RegExp(data.login),right: 1}, { _id : 0 , right : 0 , password : 0 , friends : 0 },function(err,r){
        let final = r.length != 0 ? r : false;
        socket.emit('serverRespond_FriendResult',{
          results : final
        });
      });
    }
  });

  socket.on('clientRequest_deleteFriend',(data) => {
    if(user.haveRight() && User.haveFriends(user.name,data.friendName)) {
      //User.deleteFriend(user.name,data.friendName);
      //User.deleteFriend(data.friendName,user.name);

      socket.emit('serverRespond_looseFriend',{friendName : data.friendName});
      User.sendEvents(data.friendName,'serverRespond_looseFriend',{friendName: user.name});
    }
  });

});
