var Q     = require('q');
var async = require('async');

export default class user {

  name : string;
  right : number;
  avatar : string;
  socketID : any;
  session : any;

  static io : any;
  static BDD : any;
  static allUsers : any = {};

  constructor(socketID: string,session: any) {
    this.socketID = socketID;
    this.session = session;
    this.name = session.login;
    this.right = session.right;
    this.avatar = session.avatar;
    user.allUsers[this.name] = this;
  }

  public destructor() : void {
    user.allUsers[this.name] = null;
    delete user.allUsers[this.name];
  }

  public haveRight() : boolean {
    if(this.name != undefined && this.name != null && this.right == 1) {
      return true;
    }
    console.log("User right as to be => '1'");
    return false;
  }

  static haveFriends(focus: string,friendsName : string) : any {
    let deffered = Q.defer();
    user.BDD.utilisateurs.find({login : focus}, {}, function(err,r) {
      if(err) {console.error(err);}
      if(r.length != 0) {
        let env = r[0].friends[0];
        if(env.length != 0) {
          let find = false;
          async.forEachOf(env, function(value,key, callback) {
            if(key == friendsName) {
              find = true;
              deffered.resolve(true);
              callback();
            }
          }, function(err) {
            if (err) console.error(err.message);
            if(!find) {
              deffered.resolve(false);
            }
          });
        }
        else {
          deffered.resolve(false);
        }
      }
      else {
        deffered.reject("Main:User not find");
      }
    });
    return deffered.promise;
  }

  static addFriend(focus : string, friend : string, check: boolean = false) : any {

  }

  static deleteFriend(focus : string,friend : string,check : boolean = false) : any {
    let deffered = Q.defer();
    let action = function(Ofocus,Ofriend) {
      return true;
    }
    if(check) {
      if(user.haveFriends(focus,friend)) {
        deffered.resolve(action(focus,friend));
      }
      else {
        deffered.resolve(false);
      }
    }
    else {
      deffered.resolve(action(focus,friend));
    }
    return deffered.promise;
  }

  static setBdd(bdd: any) : void {
    user.BDD = bdd;
  }

  static sendEvents(name: string,eventsName: string, o:any) : boolean {
    if(user.allUsers[name]!= undefined && user.allUsers[name] != null) {
      user.io.to(user.allUsers[name].socketID).emit(eventsName, o);
      return true;
    }
    return false;
  }

  static findUserbyName(name: string) : any {
    let deffered = Q.defer();
    user.BDD.utilisateurs.find({login : name}, {}, function(err,r) {
      if(err) {console.error(err);}
      if(r.length != 0) {
        deffered.resolve(r[0]);
      }
      else {
        deffered.reject("No user find.");
      }
    });
    return deffered.promise;
  }

  static findConnectedUser(name: string) : any {
    let deffered = Q.defer();
    deffered.resolve(user.allUsers[name] != undefined ? true : false);
    return deffered.promise;
  }

}
