var dataSchema  = require('./core/db/dataSchema');
var mongoose    = require('./core/db/conf')("localhost","27017","HUB");
var Q           = require('q');
var colors      = require('colors');
var async       = require('async');
var crypto      = require('crypto');

var sha1sum = function(input){
    return crypto.createHash('sha1').update(JSON.stringify(input)).digest('hex');
}

// Create mongoDB schema for our APP.
var createMongoSchema = function(dataStore,name) {
  var instance = new mongoose.Schema(dataStore);
  return mongoose.model(name,instance,name);
}

var users = createMongoSchema(dataSchema.utilisateurs,"utilisateurs");

// Remove all users.
var remove_AllUsers = function() {
  var deffered = Q.defer();
  users.remove({},function(err) {
    if(err) {
      deffered.reject(err);
    }
    else {
      deffered.resolve();
      console.log("Supression de tout les utilisateurs OK".magenta);
    }
  });
  return deffered.promise;
}

var save_newUsers = function(users) {
  var deffered = Q.defer();
  async.eachSeries(users, function iterator(item, callback) {
    item.save(function(err){
      if(err) {
        console.log(err);
      }
      else {
        console.log(colors.cyan('Utilisateur enregistré en BDD avec succès -> ')+"'"+colors.green(item.login)+"'")
        callback();
      }
    });
  }, function done() {
    console.log("Tout les utilisateurs sont inscrits!".bold.green);
    deffered.resolve("ok");
  });
  return deffered.promise;
}

remove_AllUsers().then(function(res) {
  var fraxken = new users({
    login : "fraxken",
    email: "gentilhomme.thomas@gmail.com",
    password: sha1sum("C@role1953"),
    avatar: "nova",
    level: 150,
    right: 1,
    cgv : false,
    cgu : false,
    active : 1,
    friends : [{
      "purexo" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "captainfive" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "shqiptar" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "pixelboy" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "elisee" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "bilou84" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }]
    }],
    blacklist : [{

    }]
  });

  var shqiptar = new users({
    login : "shqiptar",
    email: "alexandre.malaj@gmail.com",
    password: sha1sum("P@ssword0000"),
    avatar: "volt",
    level: 1,
    right: 1,
    cgv : false,
    cgu : false,
    active : 1,
    friends : [{
      "purexo" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "captainfive" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "fraxken" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "pixelboy" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "elisee" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "bilou84" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }]
    }],
    blacklist : [{

    }]
  });

  var captainfive = new users({
    login : "captainfive",
    email: "captainfive@gmail.com",
    password: sha1sum("P@ssword0000"),
    avatar: "nova2",
    level: 10,
    right: 1,
    cgv : false,
    cgu : false,
    active : 1,
    friends : [{
      "purexo" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "fraxken" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "shqiptar" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "pixelboy" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "elisee" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "bilou84" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }]
    }],
    blacklist : [{

    }]
  });

  var purexo = new users({
    login : "purexo",
    email: "contact@purexo.eu",
    password: sha1sum("P@ssword0000"),
    avatar: "ia",
    level: 95,
    right: 1,
    cgv : false,
    cgu : false,
    active : 1,
    friends : [{
      "fraxken" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "captainfive" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "shqiptar" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "pixelboy" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "elisee" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "bilou84" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }]
    }],
    blacklist : [{

    }]
  });

  var elisee = new users({
    login : "elisee",
    email: "contact@sparklinlabs.com",
    password: sha1sum("Sp@rklinlabs1234"),
    avatar: "archon",
    level: 35,
    right: 1,
    cgv : false,
    cgu : false,
    active : 1,
    friends : [{
      "fraxken" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "captainfive" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "shqiptar" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "bilou84" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "pixelboy" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }]
    }],
    blacklist : [{

    }]
  });

  var bilou84 = new users({
    login : "bilou84",
    email: "contact@sparklinlabs.com",
    password: sha1sum("Sp@rklinlabs1234"),
    avatar: "kerrigan",
    level: 40,
    right: 1,
    cgv : false,
    cgu : false,
    active : 1,
    friends : [{
      "fraxken" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "captainfive" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "shqiptar" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "elisee" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "pixelboy" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }]
    }],
    blacklist : [{

    }]
  });

  var pixelboy = new users({
    login : "pixelboy",
    email: "contact@sparklinlabs.com",
    password: sha1sum("Sp@rklinlabs1234"),
    avatar: "angel",
    level: 105,
    right: 1,
    cgv : false,
    cgu : false,
    active : 1,
    friends : [{
      "fraxken" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "captainfive" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "shqiptar" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "bilou84" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }],
      "elisee" : [{
        notification_Onlogin : true,
        notification_Onmessage : true
      }]
    }],
    blacklist : [{

    }]
  });

  save_newUsers([fraxken,shqiptar,captainfive,purexo,elisee,bilou84,pixelboy]).then(function(res) {
    process.exit();
  });

});
