module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    stylus: {
      compile: {
        options: {},
        files: {
          'public/css/main.css': 'public/production_CSS/main.styl',
          'public/css/menu.css': 'public/production_CSS/menu.styl',
          'public/css/popup/hub_options.css': 'public/production_CSS/popup/hub_options.styl',
          'public/css/popup/popup.css': 'public/production_CSS/popup/popup.styl',
          'public/css/minor/angular.css': 'public/production_CSS/minor/angular.styl',
          'public/css/minor/fix.css': 'public/production_CSS/minor/fix.styl',
          'public/css/minor/font.css': 'public/production_CSS/minor/font.styl',
          'public/css/minor/input-material.css': 'public/production_CSS/minor/input-material.styl',
          'public/css/minor/webkit.css': 'public/production_CSS/minor/webkit.styl',
          'public/css/main/friends.css': 'public/production_CSS/main/friends.styl',
          'public/css/main/group.css': 'public/production_CSS/main/group.styl',
          'public/css/main/header.css': 'public/production_CSS/main/head.styl',
        }
      }
    },
    watch: {
      stylus: {
        files: ['public/production_CSS/**/*.styl'],
        tasks: ['stylus:all'],
        options : {
          livereload: true
        },
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-newer');

  // Default task(s).
  grunt.registerTask('default', ['newer:stylus']);

};
