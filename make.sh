#!/bin/bash
execPath=$(readlink -f $(dirname $0))
cd $execPath

# Config
export PORT=4100
export DEBUG=hub

function build {
	npm install
	tsc
	grunt-cli stylus
}

function run {
	npm start
}

function clean {
        echo "êtes vous sur de vouloir reset la base de données ?"
        echo "toutes les données seront perdu ! (y/n)"
        read

        if [ $REPLY = "y" -o $REPLY = "Y" ]; then
                node database_clear.js
        fi
}

if [ "$#" = "0" ]; then
# Comportement par défaut (pas d'argument donnés au script)
	build
	run
else
	for arg in "$@"; do
		case $arg in
			build)
				build
				;;
			run)
				run
				;;
			clean)
				clean
				;;
			*)
				echo "$arg n'est pas un argument valide"
				echo "$0 {buil|run|clean}"
				echo "vous pouvez mettre plusieurs arguments, leurs fonctions seront lancé dans l'ordre que vous aurait donné"
				;;
		esac
	done
fi

