module.exports = {
  utilisateurs : {
      login: String,
      email: String,
      password: String,
      avatar: String,
      active : Number,
      cgu : Boolean,
      cgv : Boolean,
      level: Number,
      right: Number,
      friends : Array,
      blacklist : Array
  }
};
