var debug = require('debug')('hub');
var mongoose = require('mongoose');

module.exports = function(host,port,database) {
    mongoose.connect("mongodb://"+host+":"+port+"/"+database, function (error) {
        if (error) {
          debug(error);
        }
        else {
          debug("Connection a la base %s réussie avec succès",database);
        }
    });
    return mongoose;
};
