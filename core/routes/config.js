var debug = require('debug')('hub');

module.exports = (function() {
  var API = {};
  debug("Routes.config loaded");

  API.AR = function(req,res,next) {
    if(req.session.login == null || req.session.login == undefined) {
      res.redirect('/partials/auth/auth');
    }
    else {
      next();
    }
  }

  API.AC = function(req,res,next) {
    if(req.session.login == null || req.session.login == undefined) {
      next();
    }
    else {
      res.redirect('/partials/main/home');
    }
  }

  return API;
})();
