var express = require('express');

module.exports = (function() {

    var api = express.Router();
    var conf = require(__dirname+'/config')

    // Génération du pont N1
    api.get('/partials/auth/:name',conf.AC,function(req,res) {
      res.render('branch/auth',{template: req.params.name,layout: "auth"});
    });

    api.get('/partials/main/:name',conf.AR,function(req,res) {
      res.render('branch/main',{
        template: req.params.name,
        layout: "main",
        cookieAge : req.session.cookie.maxAge / 1000
      });
    });

    // Génération pont N2
    api.get('/transpile/auth/:name',conf.AC,function(req,res) {
      res.render('partials/auth/'+req.params.name);
    });

    api.get('/transpile/main/:name',conf.AR,function(req,res) {
      res.render('partials/main/'+req.params.name);
    });

    // Génération pont N3
    api.get('/ngTemplate/:name',function(req,res) {
      res.render('ngTemplates/'+req.params.name);
    });

    api.get('/popup/:name',function(req,res) {
      res.render('popup/'+req.params.name);
    });

    api.get('/',function(req,res) {
      res.render('layout');
    });

    api.get('*',function(req,res) {
      res.render('layout');
    });

    return api;
})();
