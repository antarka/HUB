var express = require('express');
var crypto = require('crypto');
var validator = require('validator');
var sha1sum = function(input){
    return crypto.createHash('sha1').update(JSON.stringify(input)).digest('hex');
}

module.exports = (function() {

    var api = express.Router();
    var conf = require(__dirname+'/config');

    api.get('/auth/userinfo',conf.AR,function(req,res) {
      res.json({
        login: req.session.login,
        email : req.session.email,
        level : req.session.level,
        avatar : req.session.avatar,
        right : req.session.right
      });
    });

    api.post('/auth/register',conf.AC,function(req,res) {
        var data = req.body.formData;
        var stackErr = [];

        if(data.login == "" || data.login == null || data.login == undefined || data.login.length < 2 || data.login.length > 30) {
          stackErr.push({"login":"Login invalide"});
        }

        if(!validator.isEmail(data.email)) {
          stackErr.push({"email":"Email invalide"});
        }

        if(!validator.matches(data.password,/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/)) {
          stackErr.push({"password":"Password invalide"});
        }

        if(stackErr.length == 0) {
          var info = {
            login : data.login,
            email : data.email,
            right:  1,
            avatar : "nova",
            level : 0,
            cgv : false,
            cgu : false,
            active: 1,
            password : sha1sum(data.password),
            friends : [{}],
            blacklist : [{}]
          }
          var newUser = new req.db.utilisateurs(info);
          newUser.save(function(err) {
            if(err)
              console.log(err)
          });
          info.password = null;
          info.cgv = null;
          info.cgu = null;
          req.session.login = info.login;
          req.session.email = info.email;
          req.session.right = info.right;
          req.session.level = info.level;
          req.session.avatar = info.avatar;
          res.json({state:"ok",nfo:info});
        }
        else {
          res.json({state:"no"});
        }

    });

    api.post('/auth/login',conf.AC,function(req,res) {
        var data = req.body.formData;
        var stackErr = [];

        if(stackErr.length == 0) {
          var cryptedPassword = sha1sum(data.password);
          req.db.utilisateurs.find({login:data.login,password:cryptedPassword},{},function(err,r) {
            if(r.length == 1) {
              var info = r[0];
              info.password = null;
              info.cgv = null;
              info.cgu = null;
              req.session.login = info.login;
              req.session.email = info.email;
              req.session.right = info.right;
              req.session.level = info.level;
              req.session.avatar = info.avatar;
              req.session.save(function(err) {
                if(err) { console.log(err) }
                res.json({state:"ok",nfo:info});
              });
            }
            else {
              res.json({state:"no"});
            }
          });
        }
    });

    // Logout function
    api.get('/logout',conf.AR,function(req,res) {
        req.session.destroy(function(err) {
            if(err) {
                debug(err);
            }
        });
        res.redirect('/');
    });

    return api;
})();
