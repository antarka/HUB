var fs = require('fs');
var dateFormat = require('dateformat');
var debug = require('debug')('hub');
var debugHeader = "logger.JS : ";

var logger = function(path, name, createpath){
	this.path = path;
	this.name = name;
	this.fileName = name + "-" + dateFormat(new Date(), "yyyy-mm-dd") + ".txt";
	this.fullPath = path + this.fileName;
	this.pathFind = false;
	this.level = {
		0: "[INFO]   ",
	 	1: "[WARNING]",
	 	2: "[MAJOR]  ",
	 	3: "[ERROR]  "
	}

	if(createpath){
		if (!fs.existsSync(this.path) ){
			debug(debugHeader+'file doesn \'t exist');
			fs.mkdirSync(this.path);
			debug(debugHeader+'path created !');
		}
		if(!fs.existsSync(this.fullPath)){
			fs.writeFile(this.fullPath, '', function(err) {
				if(err) {
	        	return debug(debugHeader+err);
	    	}
			});
		}
		this.pathFind = true;
	}else{
		if (!fs.existsSync(this.path) ){
			debug(debugHeader+'directory or file doesn \'t exist');
		}else{
			this.pathFind = true;
		}
	}
}

logger.prototype.write = function( phrase ,level){
	if(this.pathFind){
		var appendText = this.level[level] + " (" + dateFormat(new Date(),"HH:MM:ss" )+"): " + phrase + "\r\n";
		fs.appendFile(this.fullPath, appendText, function(err){
			if(err){
				debug(debugHeader+err);
			}else{
				return true;
			}
		});
	}else{
		debug(debugHeader+"Don't find path to write in log");
	}
}

module.exports = logger;
